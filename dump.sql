--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5
-- Dumped by pg_dump version 13.1

-- Started on 2021-02-02 17:16:21

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2850 (class 1262 OID 16394)
-- Name: db_studPerf; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE "db_studPerf" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'Russian_Russia.1251';


ALTER DATABASE "db_studPerf" OWNER TO postgres;

\connect "db_studPerf"

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2840 (class 0 OID 16578)
-- Dependencies: 203
-- Data for Name: city; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.city (id, name) VALUES (1, 'Moskva');
INSERT INTO public.city (id, name) VALUES (2, 'Tyumen');
INSERT INTO public.city (id, name) VALUES (3, 'Vladivostok');
INSERT INTO public.city (id, name) VALUES (4, 'Ufa');
INSERT INTO public.city (id, name) VALUES (5, 'Kirov');


--
-- TOC entry 2842 (class 0 OID 16591)
-- Dependencies: 205
-- Data for Name: college; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.college (id, name, city_id) VALUES (1, 'MGU', 1);
INSERT INTO public.college (id, name, city_id) VALUES (2, 'HSE', 1);
INSERT INTO public.college (id, name, city_id) VALUES (3, 'TyumGU', 2);
INSERT INTO public.college (id, name, city_id) VALUES (4, 'TIU', 2);
INSERT INTO public.college (id, name, city_id) VALUES (5, 'DFU', 3);
INSERT INTO public.college (id, name, city_id) VALUES (6, 'DGTRU', 3);
INSERT INTO public.college (id, name, city_id) VALUES (7, 'BGMU', 4);
INSERT INTO public.college (id, name, city_id) VALUES (8, 'BGU', 4);
INSERT INTO public.college (id, name, city_id) VALUES (9, 'VGU', 5);
INSERT INTO public.college (id, name, city_id) VALUES (10, 'VGSA', 5);


--
-- TOC entry 2844 (class 0 OID 16605)
-- Dependencies: 207
-- Data for Name: student; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.student (id, name, passport, ege, city_id, college_id) VALUES (1, 'Petrov Petr', 564123, 220, 1, 3);
INSERT INTO public.student (id, name, passport, ege, city_id, college_id) VALUES (2, 'Ivanov Ivan', 412342, 227, 1, 1);
INSERT INTO public.student (id, name, passport, ege, city_id, college_id) VALUES (3, 'Ivanov Petr', 889423, 250, 2, 3);
INSERT INTO public.student (id, name, passport, ege, city_id, college_id) VALUES (4, 'Petrov Ivan', 442374, 270, 2, 1);
INSERT INTO public.student (id, name, passport, ege, city_id, college_id) VALUES (5, 'Muslan Abul', 765327, 300, 3, 2);
INSERT INTO public.student (id, name, passport, ege, city_id, college_id) VALUES (6, 'Zhukov Artem', 125789, 220, 3, 3);
INSERT INTO public.student (id, name, passport, ege, city_id, college_id) VALUES (7, 'Adjara Gudju', 423876, 190, 4, 4);
INSERT INTO public.student (id, name, passport, ege, city_id, college_id) VALUES (8, 'Kirov Vlad', 123748, 216, 4, 5);
INSERT INTO public.student (id, name, passport, ege, city_id, college_id) VALUES (9, 'Popov Kirill', 457835, 152, 5, 5);
INSERT INTO public.student (id, name, passport, ege, city_id, college_id) VALUES (10, 'Mamaev Petr', 123786, 212, 5, 6);
INSERT INTO public.student (id, name, passport, ege, city_id, college_id) VALUES (11, 'Ivanov Sanya', 456786, 200, 4, 7);
INSERT INTO public.student (id, name, passport, ege, city_id, college_id) VALUES (12, 'Tereshenko Abdul', 453876, 162, 4, 8);
INSERT INTO public.student (id, name, passport, ege, city_id, college_id) VALUES (13, 'Sergeev Sergei', 788654, 154, 5, 9);
INSERT INTO public.student (id, name, passport, ege, city_id, college_id) VALUES (14, 'Kama Pulya', 868656, 280, 5, 10);
INSERT INTO public.student (id, name, passport, ege, city_id, college_id) VALUES (15, 'Robotov Oleg', 123458, 241, 2, 8);
INSERT INTO public.student (id, name, passport, ege, city_id, college_id) VALUES (16, 'Nursultanov Ilya', 154354, 210, 3, 9);
INSERT INTO public.student (id, name, passport, ege, city_id, college_id) VALUES (17, 'Lopouhov Daniil', 456478, 230, 4, 10);


--
-- TOC entry 2854 (class 0 OID 0)
-- Dependencies: 202
-- Name: city_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.city_id_seq', 5, true);


--
-- TOC entry 2855 (class 0 OID 0)
-- Dependencies: 204
-- Name: college_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.college_id_seq', 10, true);


--
-- TOC entry 2856 (class 0 OID 0)
-- Dependencies: 206
-- Name: student_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.student_id_seq', 17, true);


-- Completed on 2021-02-02 17:16:22

--
-- PostgreSQL database dump complete
--

