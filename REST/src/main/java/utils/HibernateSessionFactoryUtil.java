package utils;

import Models.City;
import Models.College;
import Models.Student;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

//Создание фабрики сессий приложения для работы с базой данных
public class HibernateSessionFactoryUtil {
    private static SessionFactory sessionFactory;

    private HibernateSessionFactoryUtil() {}

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                //Создание новых настроек
                Configuration configuration = new Configuration();

                //Передача созданных моделей для их восприятия базой в качество сущностей
                configuration.addAnnotatedClass(City.class);
                configuration.addAnnotatedClass(College.class);
                configuration.addAnnotatedClass(Student.class);

                //Устновка драйвера для взаимодествий с БД
                configuration.setProperty("hibernate.connection.driver_class","org.postgresql.Driver");
                //Установка конкретной БД для взаимодействия
                configuration.setProperty("hibernate.connection.url", "jdbc:postgresql://localhost:5432/db_studPerf");
                //Установка логина и пароля пользователя
                configuration.setProperty("hibernate.connection.username", "postgres");
                configuration.setProperty("hibernate.connection.password", "1");
                //Установка диалекта
                configuration.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQL9Dialect");
                //Установка типа взаимодествия с БД
                configuration.setProperty("hibernate.hbm2ddl.auto", "update");
                configuration.setProperty("hibernate.show_sql", "true");
                //Установка максимального количества подключений
                configuration.setProperty("hibernate.connection.pool_size", "10");

                //Создание сессии
                StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
                sessionFactory = configuration.buildSessionFactory(builder.build());

            } catch (Exception e) {
                System.out.println("Исключение!" + e);
            }
        }
        return sessionFactory;
    }
}
