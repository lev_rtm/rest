package com.example.MyFirstREST;

import Models.*;
import services.*;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/studsManagement")
public class StudentManagement {
    CityService cityService = new CityService();
    CollegeService collegeService = new CollegeService();
    StudentService studentService = new StudentService();

    @GET
    @Path("/students/view/{page}")
    @Produces("text/plain")
    //Вывод всех записей из таблицы Student
    public String getStudents(@PathParam("page") int page) {
        List<Student> studentList = studentService.readAll(page);

        String result = " ";
        for (Student student : studentList) {
            result = result + student.toString();
        }

        return "The result is " + result;
    }

    @POST
    @Path("/students/add")
    //Добавление новой записи в таблицу Student
    public Response addStudent(@FormParam("studentName") String studName,
                               @FormParam("passport") int pass,
                               @FormParam("ege") int ege,
                               @FormParam("cityName") String cityName,
                               @FormParam("collegeName") String collegeName) {

        if(!studName.isEmpty() && !cityName.isEmpty() && !collegeName.isEmpty()){
            List<City> cityList = cityService.findByLetter(cityName);
            List<College> collegeList = collegeService.findCollegeByName(collegeName);

            if(!cityList.isEmpty() && !collegeList.isEmpty()){
                Student newStud = new Student(studName, pass, ege);
                newStud.setCity(cityList.get(0));
                newStud.setCollege(collegeList.get(0));
                studentService.saveStudent(newStud);
                return Response.status(200)
                        .entity("Student added successfully")
                        .build();
            }

            else {
                return Response.status(200)
                        .entity("Cant add a Student because of wrong values")
                        .build();
            }
        }

        else {
            return Response.status(200)
                    .entity("Cant add a Student because of null values")
                    .build();
        }
    }

    @POST
    @Path("/students/edit")
    //Изменение записи в таблицу Student
    public Response editStud(@FormParam("id") int id,
                             @FormParam("name") String name,
                             @FormParam("passport") int pass,
                             @FormParam("ege") int ege,
                             @FormParam("cityName") String cityName,
                             @FormParam("collegeName") String collegeName){

        if (!cityName.isEmpty() || !collegeName.isEmpty()) {
            List<Student> studentList = studentService.findStudentById(id);

            if(!studentList.isEmpty()){

                if(!cityName.isEmpty()) {
                    List<City> cityList = cityService.findByLetter(cityName);

                    if(!cityList.isEmpty())
                        studentList.get(0).setCity(cityList.get(0));

                    else {
                        return Response.status(200)
                                .entity("Cant edit Student because of wrong city")
                                .build();
                    }
                }

                if(!collegeName.isEmpty()){
                    List<College> collegeList = collegeService.findCollegeByName(collegeName);

                    if(!collegeList.isEmpty())
                        studentList.get(0).setCollege(collegeList.get(0));

                    else {
                        return Response.status(200)
                                .entity("Cant edit Student because of wrong college")
                                .build();
                    }
                }

                studentList.get(0).setName(name);
                studentList.get(0).setPassport(pass);
                studentList.get(0).setEge(ege);
                studentService.updateStudent(studentList.get(0));

                return Response.status(200)
                        .entity("Student edited successfully")
                        .build();
            }

            else {
                return Response.status(200)
                        .entity("Cant find Student")
                        .build();
            }

        }

        else {
            return Response.status(200)
                    .entity("Cant add a Student because of null values")
                    .build();
        }
    }

    @GET
    @Path("/students/delete/{id}")
    //Удаление записи из таблицы Student
    public Response delStud(@PathParam("id") int id){
        List<Student> studentList = studentService.findStudentById(id);
        if (!studentList.isEmpty()){
            studentService.deleteStudent(studentList.get(0));
            return Response.status(200)
                    .entity("Student deleted successfully")
                    .build();
        }

        else{
            return Response.status(200)
                    .entity("Student deleted or not found")
                    .build();
        }

    }

    @POST
    @Path("/students/foreign")
    @Produces("text/plain")
    //Поиск иногородних студентов в университете
    public String findForeignStuds(@FormParam("collegeName") String name){
        List<College> collegeList = collegeService.findCollegeByName(name);

        if(!collegeList.isEmpty())
            return studentService.findForeignStuds(collegeList.get(0).getName());

        else {
            return ("College not found");
        }
    }

    @GET
    @Path("/students/avgEgeInCity")
    @Produces("text/plain")
    public String findAvgEgeInCitys(){
        return studentService.findAvgEgeInCitys();
    }


    @GET
    @Path("/students/minmaxege")
    @Produces("text/plain")
    public String findMinMaxEge(){
        return studentService.findMinMaxEge();
    }

    @POST
    @Path("/students/findByName")
    @Produces("text/plain")
    public String findByName(@FormParam(value = "name") String name){
        List<Student> studentList = studentService.findStudentByName(name);

        if(!studentList.isEmpty()){
            String result = " ";

            for (Student student : studentList) {
                result = result + student.toString();
            }

            return "The result is " + result;
        }

        else {
            return "Student not found";
        }
    }
}
