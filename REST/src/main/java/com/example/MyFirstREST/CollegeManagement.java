package com.example.MyFirstREST;

import Models.City;
import Models.College;
import services.CollegeService;
import services.CityService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/collegeManagement")
public class CollegeManagement {
    CollegeService collegeService = new CollegeService();
    CityService cityService = new CityService();

    @GET
    @Path("/colleges/view/{page}")
    @Produces("text/plain")
    //Вывод информации о записях в таблице College
    public String getColleges(@PathParam("page") int page) {
        List<College> collegeList = collegeService.readAll(page);

        String result = " ";
        for (College college : collegeList) {
            result = result + college.toString();
        }

        return "The result is " + result;
    }

    @POST
    @Path("/colleges/add")
    //Добавление новой записи в таблицу College
    public Response addCollege(@FormParam("collegeName") String colName, @FormParam("cityName") String cityName){
        if (!colName.isEmpty() && !cityName.isEmpty()) {
            List<City> cityList = cityService.findByLetter(cityName);

            if (cityList.isEmpty()) {
                return Response.status(200)
                        .entity("Cant add college : " + colName)
                        .build();
            }
            else {
                College newCollege = new College(colName);
                newCollege.setCity(cityList.get(0));
                collegeService.saveCollege(newCollege);
                return Response.status(200)
                        .entity("College added : " + colName)
                        .build();
            }
        }

        else {
            return Response.status(200)
                    .entity("Cant add college because of null parameters")
                    .build();
        }
    }

    @POST
    @Path("/colleges/edit")
    //Изменение записи в таблицу College
    public Response editCollege(@FormParam("id") int id, @FormParam("newName") String newName, @FormParam("newCity") String newCity) {
        if (!newName.isEmpty() || !newCity.isEmpty()) {
            List<College> collegeList = collegeService.findCollegeById(id);

            if (!collegeList.isEmpty()) {

                if (!newName.isEmpty())
                    collegeList.get(0).setName(newName);

                if (!newCity.isEmpty()) {
                    List<City> nCity= cityService.findByLetter(newCity);

                    if (!nCity.isEmpty())
                        collegeList.get(0).setCity(nCity.get(0));

                    else {
                        return Response.status(200)
                                .entity("Cant set new City")
                                .build();
                    }
                }
                collegeService.updateCollege(collegeList.get(0));
                return Response.status(200)
                        .entity("College edited successfully")
                        .build();
            }

            else {
                return Response.status(200)
                        .entity("College not found")
                        .build();
            }
        }

        else {
            return Response.status(200)
                    .entity("No new values")
                    .build();
        }
    }

    @GET
    @Path("/colleges/delete/{id}")
    //Удаление записи из таблицы Colleges
    public Response delCollege(@PathParam("id") int id){
        List<College> collegeList = collegeService.findCollegeById(id);
        if (collegeList.isEmpty()){
            return Response.status(200)
                    .entity("College deleted or not found")
                    .build();
        }
        else {
            collegeService.deleteCollege(collegeList.get(0));
            return Response.status(200)
                    .entity("College deleted successfully")
                    .build();
        }
    }

    @POST
    @Path("/colleges/findStuds")
    @Produces("text/plain")
    //Поиск всех студентов в университете по его названию
    public String findStuds(@FormParam("name") String name){
        if (collegeService.findStudsInCollege(name) != null) {
            return collegeService.findStudsInCollege(name);
        }
        else {return "Nothing found";}
    }

    @POST
    @Path("/colleges/findColleges")
    @Produces("text/plain")
    //Поиск универститета по его названию
    public String findColleges(@FormParam("name") String name){
        if (collegeService.findCollegesInCity(name) != null) {
            return collegeService.findCollegesInCity(name);
        }
        else {return "Nothing found";}
    }

    @POST
    @Path("/colleges/findBestStudents")
    @Produces("text/plain")
    //Поиск студентов университета с максимальным баллом среди других студентов этого универститета
    public String findBestStudents(@FormParam("name") String name){
        if (collegeService.findBestStudsInCollege(name) != null) {
            return collegeService.findBestStudsInCollege(name);
        }
        else {return "Nothing found";}
    }

    @POST
    @Path("/colleges/findCollege")
    @Produces("text/plain")
    //Поиск университета по его названию
    public String collegeByName(@FormParam(value = "name") String name){
        List<College> collegeList = collegeService.findCollegeByName(name);

        if (collegeList.isEmpty()){
            return "College not found";
        }

        else {
            String result = " ";
            for (College college : collegeList) {
                result = result + college.toString();
            }

            return "The result is " + result;
        }
    }
}
