package com.example.MyFirstREST;

import Models.City;
import services.CityService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/cityManagment")
public class CityManagement {

    CityService cityService = new CityService();

    @GET
    @Path("/citys/view/{page}")
    @Produces("text/plain")
    //Вывод информации о записях в таблице City
    public String getCitys(@PathParam("page") int page) {
        List<City> cityList = cityService.readAll(page);
        String result = " ";
        for (City city : cityList) {
            result = result + city.toString();
        }
        return "The result is " + result;
    }

    @POST
    @Path("/citys/add")
    //Добавление новой записи в таблицу City
    public Response addCity(@FormParam("name") String name){
        cityService.saveCity(new City(name));
        return Response.status(200)
                .entity("addCity is called, name : " + name)
                .build();
    }

    @POST
    @Path("/citys/findCity")
    @Produces("text/plain")
    //Поиск города по его название
    public String cityByName(@FormParam(value = "name") String name){
        List<City> cityList = cityService.findByLetter(name);

        if (cityList.isEmpty()){
            return "City not found";
        }

        else {
            String result = " ";
            for (City city : cityList) {
                result = result + city.toString();
            }

            return "The result is " + result;
        }
    }

    @POST
    @Path("/citys/edit")
    //Изменение записи в таблице City
    public Response editCity(@FormParam("id") int id, @FormParam("name") String name){
        List<City> cityList = cityService.findById(id);

        if (cityList.isEmpty()){
            return Response.status(200)
                    .entity("City not found")
                    .build();
        }

        else {
            cityList.get(0).setName(name);
            cityService.updateCity(cityList.get(0));
            return Response.status(200)
                    .entity("city name edited successfully")
                    .build();
        }
    }

    @GET
    @Path("/citys/delete/{id}")
    //Удаление записи в таблице City
    public Response delCity(@PathParam("id") int id){
        List<City> cityList = cityService.findById(id);

        if (cityList.isEmpty()){
            return Response.status(200)
                    .entity("City deleted or not found")
                    .build();
        }

        else {
            cityService.deleteCity(cityList.get(0));
            return Response.status(200)
                    .entity("City deleted successfully")
                    .build();
        }
    }

    @POST
    @Path("/citys/findStuds")
    @Produces("text/plain")
    //Поиск студентов в заданном городе с баллами выше переданных
    public String findGoodStud(@FormParam("points") int pts, @FormParam("name") String name){

        if (cityService.findGoodStudents(pts, name) != null) {
            return cityService.findGoodStudents(pts, name);
        }

        else {return "Nothing found";}
    }
}
