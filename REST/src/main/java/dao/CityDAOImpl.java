package dao;

import Models.City;
import utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import org.hibernate.query.Query;
import java.util.List;

//Реализация интерфейса доступа к данным таблицы City
public class CityDAOImpl implements CityDAO{

    //Вывод всех записей в талице City, с пагинацией
    public List<City> findAll(int page) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();

        String hql = "From City order by name asc";
        Query query = session.createQuery(hql);

        int firstRes = page * 2;
        query.setFirstResult(firstRes);
        query.setMaxResults(2);
        List<City> citys = query.list();

        return citys;
    }

    //Поиск всех студентов в заданном городе, у которых баллы за ЕГЭ выше указанных
    public String findStudents(int points, String name) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        String hql = "select c.name, s.name, s.ege " +
                "from Student s join s.city c " +
                "where s.ege >= :points and c.name = :name order by s.ege desc";

        Query query = session.createQuery(hql);
        query.setParameter("name", name);
        query.setParameter("points", points);
        List<Object[]> rows = query.list();
        session.close();

        String result = "City name | Student name | EGE points";
        for(Object[] row : rows) {
            result += "\n" + row[0].toString() + " | " + row[1].toString() + " | " + row[2].toString();
        }
        return result;
    }

    //Сохранения новой записи в таблице City
    public void save(City city) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction trans = session.beginTransaction();
        session.save(city);
        trans.commit();
        session.close();
    }

    //Обновление записи в таблице City
    public void update(City city) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction trans = session.beginTransaction();
        session.update(city);
        trans.commit();
        session.close();
    }

    //Удаление записи из таблицы City
    public void delete(City city) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction trans = session.beginTransaction();

        String hql = "DELETE " +
                "from City c " +
                "where c.id = :id";
        Query query = session.createQuery(hql);
        query.setParameter("id", city.getId());
        query.executeUpdate();

        trans.commit();
        session.close();
    }

    //Поиск города по его названию
    public List<City> findByName(String name) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();

        String hql = "select c " +
                       "from City c " +
                       "where c.name like :name";
        Query query = session.createQuery(hql);
        query.setParameter("name", name);

        List<City> citys = query.list();
        session.close();

        return citys;
    }

    //Поиск города по его id
    public List<City> findById(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();

        String hql = "select c " +
                "from City c " +
                "where c.id = :id";
        Query query = session.createQuery(hql);
        query.setParameter("id", id);

        List<City> citys = query.list();
        session.close();
        return citys;
    }
}
