package dao;

import Models.College;

import java.util.List;

//Интерфейс доступа к данным таблицы College
public interface CollegeDAO {
    List<College> findAll(int page);
    String findAllStudents(String colName);
    void save(College college);
    void update(College college);
    void delete(College college);
    String findByCity(String cityName);
    String findBestStudents(String colName);
    List<College> findByName(String colName);
    List<College> findById(int id);
}
