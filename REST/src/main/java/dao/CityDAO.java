package dao;

import Models.City;

import java.util.List;

//Интерфейс доступа к данным таблицы City
public interface CityDAO {
    List<City> findAll(int page);
    String findStudents(int points, String cityName);
    void save(City city);
    void update(City city);
    void delete(City city);
    List<City> findByName(String cityName);
    List<City> findById(int id);
}
