package dao;

import Models.Student;

import java.util.List;

//Интерфейс доступа к данным таблицы Student
public interface StudentDAO {
    List<Student> findAll(int page);
    void save(Student student);
    void update(Student student);
    void delete(Student student);
    String findForeignStudents(String collegeName);
    String findAvgEGEByCity();
    String findMinMaxEGE();
    List<Student> findByName(String cityName);
    List<Student> findById(int id);
}
