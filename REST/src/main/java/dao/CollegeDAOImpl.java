package dao;

import Models.College;
import utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import org.hibernate.query.Query;
import java.util.List;

//Реализация интерфейса доступа к данным таблицы College
public class CollegeDAOImpl implements CollegeDAO{

    //Вывод всех записей в талице College, с пагинацией
    public List<College> findAll(int page) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();

        String hql = "From College order by name asc";
        Query query = session.createQuery(hql);

        int firstRes = page * 2;
        query.setFirstResult(firstRes);
        query.setMaxResults(2);
        List<College> colleges = query.list();

        return colleges;
    }

    //Поиск всех студентов в университете
    public String findAllStudents(String colName) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        String hql = "select c.name, s.name, s.passport " +
                "from Student s " +
                "join s.college c " +
                "where c.name = :name";

        Query query = session.createQuery(hql);
        query.setParameter("name", colName);
        List<Object[]> rows = query.list();
        session.close();

        String result = "College name | Student name | Student passport";
        for(Object[] row : rows) {
            result += "\n" + row[0].toString() + " | " + row[1].toString() + " | " + row[2].toString();
        }

        return result;
    }

    //Добавление записи в таблицу College
    public void save(College college) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction trans = session.beginTransaction();
        session.save(college);
        trans.commit();
        session.close();
    }

    //Обновление записи в таблице College
    public void update(College college) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction trans = session.beginTransaction();
        session.update(college);
        trans.commit();
        session.close();
    }

    //Удаление записи из таблицы College
    public void delete(College college) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction trans = session.beginTransaction();

        String hql = "DELETE " +
                "from College c " +
                "where c.id = :id";
        Query query = session.createQuery(hql);
        query.setParameter("id", college.getId());
        query.executeUpdate();

        trans.commit();
        session.close();
    }

    //Поиск всех университетов в городе
    public String findByCity(String cityName) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();

        String hql = "select c.name, col.name " +
                "from City c " +
                "join c.colleges col " +
                "where c.name = :name " +
                "order by col.name asc ";
        Query query = session.createQuery(hql);
        query.setParameter("name", cityName);
        List<Object[]> rows = query.list();
        session.close();

        String result = "City name | College name";
        for(Object[] row : rows) {
            result += "\n" + row[0].toString() + " | " + row[1].toString();
        }

        return result;
    }

    //Поиск студентов университета, у которых баллы за ЕГЭ выше среднего по университету
    public String findBestStudents(String colName) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();

        String hql = "select col.name, s.name, s.city.name, s.ege " +
                "from College col " +
                "join col.students s " +
                "where col.name = :name and " +
                "s.ege >= (select avg(s2.ege) from College col2 join col2.students s2 where col2.name = :name) " +
                "order by s.name asc ";
        Query query = session.createQuery(hql);
        query.setParameter("name", colName);
        List<Object[]> rows = query.list();
        session.close();

        String result = "College name | Student name | Hometown | EGE points";
        for(Object[] row : rows) {
            result += "\n" + row[0].toString() + " | " + row[1].toString() + " | " + row[2].toString() + " | " + row[3].toString();
        }

        return result;
    }

    //Поиск университета по его названию
    public List<College> findByName(String name) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        String hql = "select c " +
                "from College c " +
                "where c.name like :name";
        Query query = session.createQuery(hql);
        query.setParameter("name", name);
        List<College> colleges = query.list();
        session.close();
        return colleges;
    }

    //Поиск университета по его id
    public List<College> findById(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        String hql = "select c " +
                "from College c " +
                "where c.id = :id";
        Query query = session.createQuery(hql);
        query.setParameter("id", id);
        List<College> colleges = query.list();
        session.close();
        return colleges;
    }
}
