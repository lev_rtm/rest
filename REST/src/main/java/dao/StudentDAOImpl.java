package dao;

import Models.Student;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import utils.HibernateSessionFactoryUtil;

import java.util.List;

//Реализация интерфейса доступа к данным таблицы Student
public class StudentDAOImpl implements StudentDAO{

    //Вывод всех записей в талице Student, с пагинацией
    public List<Student> findAll(int page) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();

        String hql = "From Student order by name asc";
        Query query = session.createQuery(hql);

        int firstRes = page * 5;
        query.setFirstResult(firstRes);
        query.setMaxResults(5);
        List<Student> students = query.list();

        return students;
    }

    //Добавление новой записи в таблицу Student
    public void save(Student student) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction trans = session.beginTransaction();
        session.save(student);
        trans.commit();
        session.close();
    }

    //Обновление записи в таблице Student
    public void update(Student student) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction trans = session.beginTransaction();
        session.update(student);
        trans.commit();
        session.close();
    }

    //Удаление записи из таблицы Student
    public void delete(Student student) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction trans = session.beginTransaction();

        String hql = "DELETE " +
                "from Student s " +
                "where s.id = :id";
        Query query = session.createQuery(hql);
        query.setParameter("id", student.getId());
        query.executeUpdate();

        trans.commit();
        session.close();
    }

    //Поиск иногородних студентов в университете
    public String findForeignStudents(String collegeName) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();

        String hql = "select col.name, s.name, s.city.name " +
                "from College col " +
                "join col.students s " +
                "where col.name = :name and s.city.name != col.city.name " +
                "order by s.name asc ";
        Query query = session.createQuery(hql);
        query.setParameter("name", collegeName);
        List<Object[]> rows = query.list();
        session.close();

        String result = "College name | Student name | Student Hometown";
        for(Object[] row : rows) {
            result += "\n" + row[0].toString() + " | " + row[1].toString() + " | " + row[2].toString();
        }

        return result;
    }

    //Поиск среднего ЕГЭ по всем городам
    public String findAvgEGEByCity() {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();

        String hql = "select c.name, avg(s.ege) as aver " +
                "from City c " +
                "join c.students s " +
                "group by c.name " +
                "order by aver asc ";
        Query query = session.createQuery(hql);
        List<Object[]> rows = query.list();
        session.close();

        String result = "City name | AVG points";
        for(Object[] row : rows) {
            result += "\n" + row[0].toString() + " | " + row[1].toString();
        }

        return result;
    }

    //Поиск минимального и максимального ЕГЭ среди всех студентов
    public String findMinMaxEGE() {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();

        String hql = "select min(s.ege), max(s.ege) " +
                "from Student s ";
        Query query = session.createQuery(hql);
        List<Object[]> rows = query.list();
        session.close();

        String result = "Min points | Max points";
        for(Object[] row : rows) {
            result += "\n" + row[0].toString() + " | " + row[1].toString();
        }

        return result;
    }

    //Поиск студента по его ФИО
    public List<Student> findByName(String studentName) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        String hql = "select s " +
                "from Student s " +
                "where s.name like :name";
        Query query = session.createQuery(hql);
        query.setParameter("name", studentName);
        List<Student> students = query.list();
        session.close();
        return students;
    }

    //Поиск студента по его id
    public List<Student> findById(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        String hql = "select s " +
                "from Student s " +
                "where s.id = :id";
        Query query = session.createQuery(hql);
        query.setParameter("id", id);
        List<Student> students = query.list();
        session.close();
        return students;
    }
}
