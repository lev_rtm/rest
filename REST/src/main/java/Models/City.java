package Models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

//Модель для сущности City
@Entity
@Table (name = "city")
public class City {
    //Первичный ключ
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    //Название города
    private String name;

    //Связь с сущностью Student
    //В одном городе может находится множество студентов
    @OneToMany(mappedBy = "city", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Student> students;

    //Связь с сущностью College
    //В одном городе может находится множество университетов
    @OneToMany(mappedBy = "city", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<College> colleges;

    //Конструктор по умолчанию
    public City(){

    }

    //Конструктор с параметрами
    public City(String name){
        this.name = name;
        students = new ArrayList<>();
        colleges = new ArrayList<>();
    }

    //Добавление в коллекцию студентов
    public void addStudent(Student student){
        student.setCity(this);
        students.add(student);
    }

    //Удаление из коллекции студентов
    public void removeStudent(Student student){
        students.remove(student);
    }

    //Добавление в коллекцию университетов
    public void addCollege(College college){
        college.setCity(this);
        colleges.add(college);
    }

    //Удаление из коллекции университетов
    public void removeCollege(College college){
        colleges.remove(college);
    }

    //Свойство для получения id города
    public int getId(){
        return id;
    }

    //Свойство для получения названия города
    public String getName(){
        return name;
    }

    //Свойство для изменения названия города
    public void setName(String name){
        this.name = name;
    }

    //Свойство для получения коллекции студентов
    public List<Student> getStudents(){
        return students;
    }

    //Свойсвтво для установки коллекции студентов
    public void setStudents(List<Student> students){
        this.students = students;
    }

    //Свойстов для получения коллекции университетов
    public List<College> getColleges(){
        return colleges;
    }

    //Свойсвтво для установки коллекции университетов
    public void setColleges(List<College> colleges){
        this.colleges = colleges;
    }

    //Переопределение метода вывода информации о городе
    @Override
    public String toString() {
//        String studs = "";
//        if ((students != null) && (students.size() > 0)) {
//            for (int i = 0; i < students.size(); i++) {
//                if (i > 0)
//                    studs += ",";
//                studs += students.get(i).toString();
//            }
//        }
        return "\nid =" + id + ", Nazvanie = " + name; //+ " {students = [" + studs + "]}";
    }
}
