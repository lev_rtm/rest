package Models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

//Модель для сущности College
@Entity
@Table (name = "college")
public class College {

    //Первичный ключ
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    //Название университета
    private String name;

    //Связь с сущностью City
    //Множество университетов может находится в одном городе
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "city_id")
    private City city;

    //Связь с сущностью Student
    //В одном университете может находится множетсво студентов
    @OneToMany(mappedBy = "college", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Student> students;

    //Конструктор по умолачнию
    public College(){

    }

    //Констуктор с параметрами
    public College(String name){
        this.name = name;
        students = new ArrayList<>();
    }

    //Добавлению в коллекцию студентов
    public void addStudent(Student student){
        student.setCollege(this);
        students.add(student);
    }

    //Удаление из коллекции студентов
    public void removeStudent(Student student){
        students.remove(student);
    }

    //Свойство для получения id
    public int getId(){
        return id;
    }

    //Свойство для получения названия университета
    public String getName(){
        return name;
    }

    //Свойство для установки названия университета
    public void setName(String name){
        this.name = name;
    }

    //Свойство для получения коллекции студентов университета
    public List<Student> getStudents(){
        return students;
    }

    //Свойство для установки коллекции студентов университета
    public void setStudents(List<Student> students){
        this.students = students;
    }

    //Свойстов для получения города университета
    public City getCity(){
        return city;
    }

    //Свойство для установки города университета
    public void setCity(City city){
        this.city = city;
    }

    //Переопределение метода получения информации об университете
    @Override
    public String toString() {
        return "\nid =" + id + ", College name = " + name;
    }
}
