package Models;

import javax.persistence.*;

//Модель для сущности Student
@Entity
@Table (name = "student")
public class Student {

    //Первичный ключ
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    //ФИО студента
    private String name;

    //Пассппортные данные студента
    private int passport;

    //Баллы за ЕГЭ
    private int ege;

    //Связь с сущностью City
    //Множество студентов может находится в одном городе
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "city_id")
    private City city;

    //Связь с сущностью College
    //Множестов студентов может находится в одном университете
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "college_id")
    private College college;

    //Конструктор по умолчанию
    public Student(){

    }

    //Конструктор с параметрами
    public Student(String name, int passport, int EGE_points){
        this.name = name;
        this.passport = passport;
        this.ege = EGE_points;
    }

    //Свойство для получения id
    public int getId(){
        return id;
    }

    //Свойство для получения ФИО студента
    public String getName(){
        return name;
    }

    //Свойстов для изменения ФИО студента
    public void setName(String name){
        this.name = name;
    }

    //Свойстов для получения пасспорта студента
    public int getPassport(){
        return passport;
    }

    //Свойстов для изменения паспорта студента
    public void setPassport(int passport){
        this.passport = passport;
    }

    //Свойстов для получения баллов ЕГЭ
    public int getEge(){
        return ege;
    }

    //Свойстов для изменения баллов ЕГЭ
    public void setEge(int ege){
        this.ege = ege;
    }

    //Свойство для получения города студента
    public City getCity(){
        return city;
    }

    //Свойство для установки города студента
    public void setCity(City city){
        this.city = city;
    }

    //Свойстов для получения университета студента
    public College getCollege(){
        return college;
    }

    //Свойстов для установки университетат студента
    public void setCollege(College college){
        this.college = college;
    }

    //Переопределение метода получения информации о студенте
    @Override
    public String toString() {
        return "\nid = " + id +
                ", FIO = " + name +
                ", Passport = " + passport +
                ", EGE points = " + ege;
    }
}
