package services;

import dao.StudentDAOImpl;
import Models.Student;
import java.util.List;

//Слой данных для выполнения бизнес-логики таблицы Student
public class StudentService {
    private StudentDAOImpl studentDAO = new StudentDAOImpl();

    public StudentService(){

    }

    public List<Student> readAll(int page){ return studentDAO.findAll(page); }

    public void saveStudent(Student student) { studentDAO.save(student); }

    public void updateStudent(Student student) { studentDAO.update(student); }

    public void deleteStudent(Student student) { studentDAO.delete(student); }

    public String findForeignStuds(String collegeName) { return studentDAO.findForeignStudents(collegeName); }

    public String findAvgEgeInCitys() { return studentDAO.findAvgEGEByCity(); }

    public String findMinMaxEge() { return studentDAO.findMinMaxEGE(); }

    public List<Student> findStudentByName(String studName) { return studentDAO.findByName(studName); }

    public List<Student> findStudentById(int id) { return studentDAO.findById(id); }
}
