package services;

import dao.CollegeDAOImpl;
import Models.College;
import java.util.List;

//Слой данных для выполнения бизнес-логики таблицы College
public class CollegeService {

    private CollegeDAOImpl collegeDAO = new CollegeDAOImpl();

    public CollegeService(){

    }

    public List<College> readAll(int page) { return collegeDAO.findAll(page); }

    public void saveCollege(College college) {
        collegeDAO.save(college);
    }

    public void updateCollege(College college) {
        collegeDAO.update(college);
    }

    public void deleteCollege(College college) {
        collegeDAO.delete(college);
    }

    public String findStudsInCollege(String collegeName) { return collegeDAO.findAllStudents(collegeName); }

    public String findCollegesInCity(String cityName) { return collegeDAO.findByCity(cityName); }

    public String findBestStudsInCollege(String collegeName) { return collegeDAO.findBestStudents(collegeName); }

    public List<College> findCollegeByName(String collegeName) { return collegeDAO.findByName(collegeName); }

    public List<College> findCollegeById(int id) { return collegeDAO.findById(id); }
}
