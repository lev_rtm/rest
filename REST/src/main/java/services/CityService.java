package services;

import dao.CityDAOImpl;
import Models.City;
import java.util.List;

//Слой данных для выполнения бизнес-логики таблицы City
public class CityService {

    private CityDAOImpl cityDAO = new CityDAOImpl();

    public CityService(){

    }

    public List<City> readAll(int page) { return cityDAO.findAll(page); }

    public void saveCity(City city) {
        cityDAO.save(city);
    }

    public void deleteCity(City city) { cityDAO.delete(city); }

    public void updateCity(City city) {
        cityDAO.update(city);
    }

    public String findGoodStudents(int minPoints, String cityName) { return cityDAO.findStudents(minPoints, cityName); }

    public List<City> findByLetter(String name) {
        return cityDAO.findByName(name);
    }

    public List<City> findById(int id) {
        return cityDAO.findById(id);
    }
}
